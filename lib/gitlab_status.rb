require "gitlab_status/version"
require "net/http"
require "benchmark"
require "ruby-progressbar"

module GitlabStatus
  class Report
    TOTAL_TIME_CHECK_SEC = 60
    PERIODICAL_CHECK_SEC = 10
    URL = "https://about.gitlab.com"

    def initialize
      @success_counter = 0
      @error_counter = 0
      @request_times = []
      @num_of_requests = TOTAL_TIME_CHECK_SEC / PERIODICAL_CHECK_SEC
    end

    def check_status
      progressbar = create_progressbar

      @num_of_requests.times do |counter|
        progressbar.increment
        @request_times << Benchmark.realtime { count_request_statuses }
        sleep(PERIODICAL_CHECK_SEC)
      end

      progressbar.finish

      {
        availability_percentage: availability_percentage,
        average_time_per_request: average_time
      }
    end

    private

    def send_request
      uri = URI(URL)
      Net::HTTP.get_response(uri)
    end

    def availability_percentage
      down_time_sec = @error_counter * PERIODICAL_CHECK_SEC
      ((TOTAL_TIME_CHECK_SEC - down_time_sec) * 100) / TOTAL_TIME_CHECK_SEC.to_f
    end

    def average_time
      (@request_times.sum / @request_times.length).round(2)
    end

    def count_request_statuses
      case send_request
      when Net::HTTPSuccess, Net::HTTPRedirection
        @success_counter += 1
      else
        @error_counter += 1
      end
    end

    def create_progressbar
      ::ProgressBar.create(
        title: "Sending Requests",
        starting_at: 0,
        total: @num_of_requests,
        format: "%t: %B %j%%"
      )
    end
  end
end
