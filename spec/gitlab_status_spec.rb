RSpec.describe GitlabStatus::Report do
  subject(:report) { described_class.new }

  before do
    allow(subject)
      .to receive(:create_progressbar)
      .and_return(double(finish: true, increment: true))
  end

  describe "#check_status" do
    it "sends 6 requests to check the availability" do
      stub_const("TOTAL_TIME_CHECK_SEC", 60)
      stub_const("PERIODICAL_CHECK_SEC", 10)

      allow(report).to receive(:sleep).and_return(true)
      expect(report).to receive(:send_request).exactly(6).times
      report.check_status
    end

    it 'returns an availability of 100%' do
      stub_const("TOTAL_TIME_CHECK_SEC", 60)
      stub_const("PERIODICAL_CHECK_SEC", 10)
      stub_request(:any, described_class::URL)
      allow(report).to receive(:sleep).and_return(true)

      status = report.check_status
      expect(status[:availability_percentage]).to eq(100.0)
      expect_average_time_per_request(status)
    end

    it 'returns an availability of 0%' do
      stub_const("TOTAL_TIME_CHECK_SEC", 60)
      stub_const("PERIODICAL_CHECK_SEC", 10)
      stub_request(:any, described_class::URL).to_return(status: 500)
      allow(report).to receive(:sleep).and_return(true)

      status = report.check_status
      expect(status[:availability_percentage]).to eq(0.0)
      expect_average_time_per_request(status)
    end

    it "returns availability of 66%" do
      stub_const("TOTAL_TIME_CHECK_SEC", 60)
      stub_const("PERIODICAL_CHECK_SEC", 10)
      stub_request(:any, described_class::URL)
        .to_return({ status: 500 }, { status: 500 }, { status: 200 })
      allow(report).to receive(:sleep).and_return(true)

      status = report.check_status
      expect(status[:availability_percentage]).to be_within(0.1).of(66.6)
      expect_average_time_per_request(status)
    end
  end

  def expect_average_time_per_request(status)
    expect(status[:average_time_per_request]).to be_within(0.01).of(0.001)
  end
end
