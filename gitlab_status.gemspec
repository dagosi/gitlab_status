
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "gitlab_status/version"

Gem::Specification.new do |spec|
  spec.name          = "gitlab_status"
  spec.version       = GitlabStatus::VERSION
  spec.authors       = ["Daniel Gomez-Sierra"]
  spec.email         = ["danielgomezsierra@gmail.com"]

  spec.summary       = %q{GitLab Status}
  spec.description   = %q{Check the status of http://gitlab.com}
  spec.homepage      = "https://gitlab.com/dagosi/gitlab_status"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  # if spec.respond_to?(:metadata)
  #   spec.metadata["allowed_push_host"] = "Set to 'http://mygemserver.com'"
  # else
  #   raise "RubyGems 2.0 or newer is required to protect against " \
  #     "public gem pushes."
  # end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "bin"
  spec.executables   = ["gitlab-status"]
  spec.require_paths = ["lib"]

  spec.add_dependency "ruby-progressbar", "~> 1.9.0"
  
  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "webmock", "~> 3.3.0"
  spec.add_development_dependency 'pry-byebug', '~> 3.4'
end
